package com.thanakit;

public class Rectangle {
    public int width;
    public int height;
    public Rectangle(int width, int height){
        this.height = height;
        this.width = width;
    }

    public int area(){
        int ares = width*height;
        return ares;
    }
    public int perimeter(){
        int perimeter = width+width+height+height;
        return perimeter;
    }

}
