package com.thanakit;

public class TestShape {
    public static void main( String[] args ){

        // object
        Rectangle Rect1 = new Rectangle(10, 5);
        Rectangle Rect2 = new Rectangle(5, 3);
        circle circle1 = new circle(1);
        circle circle2 = new circle(2);
        triangle triangle1 = new triangle(5, 5, 6);


        //print area
        System.out.println("Rect1 area :"+Rect1.area());
        System.out.println("Rect2 area :"+Rect2.area());
        System.out.println("circle1 area :"+circle1.area());
        System.out.println("circle2 area :"+circle2.area());
        System.out.println("triangle1 area :"+triangle1.area());
        // print perimeter
        System.out.println("Rect1 perimeter :"+Rect1.perimeter());
        System.out.println("Rect2 perimeter :"+Rect2.perimeter());
        System.out.println("circle1 perimeter :"+circle1.perimeter());
        System.out.println("circle2 perimeter :"+circle2.perimeter());
        System.out.println("triangle1 perimeter :"+triangle1.perimeter());
        
        
    }
}
