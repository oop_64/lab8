package com.thanakit;

public class Tree {
    private int x;
    private int y;
    public Tree(int x,int y){
        this.x = x;
        this.y = y;
    }
    public int GetX(){
        return x;
    }
    public int GetY(){
        return y;
    }

    public void info(){
        System.out.print(" X :"+GetX()+" Y :"+GetY());
    }
}
