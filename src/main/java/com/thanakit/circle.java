package com.thanakit;

public class circle {
    public int radius;

    public circle(int radius){
        this.radius = radius;
    }

    public double area(){
        double ares = Math.PI*(Math.pow(radius,2));
        return ares;
    }
    
    public double perimeter(){
        double perimeter = (2*(Math.PI)*radius);
        return perimeter;
    }
}
